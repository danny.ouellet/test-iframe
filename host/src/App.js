
import './App.css';

function App() {
  return (
    <div className="App">
      <p><button onClick={() => { window.location = "https://child.localhost:6002" }}>Step 1 (Redirect to Child)</button></p>
      <iframe src="https://child.localhost:6002/" width="800" height="600" sandbox="allow-storage-access-by-user-activation allow-scripts allow-same-origin allow-modals" />
      <p><button onClick={() => { window.location = "https://child.localhost:6002" }}>Step 4 (Redirect to Child to see cookie)</button></p>
    </div>
  );
}

export default App;
