import './App.css';

import Cookies from 'js-cookie'
import { useEffect, useRef } from 'react';


function App() {




  function setTheCookie() {
    // Let's access some items from the first-party cookie jar
    Cookies.set('foo', 'COOKIE VALUE')
    localStorage.setItem("foo", "LOCAL STORAGE VALUE"); // access a localStorage entry
  }

  const handleSetCookie = () => {
    if (document.hasStorageAccess == null) {
      // This browser doesn't support the Storage Access API, so let's just hope we have access!
      setTheCookie();
    } else {
      document.hasStorageAccess().then((hasAccess) => {
        if (hasAccess) {
          // We already have access, so let's do things right away!
          setTheCookie();
        } else {
          // As we don't have access, we need to request it. This request has to happen within
          // an event handler for a user interaction (e.g., clicking)
          document.requestStorageAccess().then(() => {
            setTheCookie();
          }).catch((err) => {
            // If there is an error obtaining storage access.
            console.error('Error obtaining storage access', err);
          })
          
        }
      })
    }
  }
  function readTheCookie() {
    // Let's access some items from the first-party cookie jar
    alert(`Cookie : ${Cookies.get('foo')} Local storage : ${localStorage.getItem("foo")}`);
  }


  
  const refButton = useRef();

  useEffect(() => {
    if(refButton.current){
      refButton.current.addEventListener('click', () => {
        document.requestStorageAccess().then(() => {
          readTheCookie();
        }).catch((err) => {
          // If there is an error obtaining storage access.
          console.error('Error obtaining storage access', err);
          readTheCookie();
        })
      })
    }
  })

  return (
    <div className="App">
      <header className="App-header">
        <p><button onClick={handleSetCookie}>Step 2 (Set the cookie and localStorage)</button></p>
        <p><button onClick={() => { window.location = "https://host.localhost:6001" }}>Step 3 (Redirect to Host)</button></p>
        <p><button ref={refButton}>Step 4 (Read the cookie)</button></p>
      </header>
    </div>
  );
}

export default App;
