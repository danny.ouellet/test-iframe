# Install
- yarn i

## Configure local hosts
```
127.0.0.1 host.localhost
127.0.0.1 child.localhost
```

# Run
- yarn start
- open https://host.localhost:6001/
- Follow steps number
